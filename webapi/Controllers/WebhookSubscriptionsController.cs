﻿using AutoMapper;
using HMACAuthenticationWebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using webapi.Data.Repositories;
using webapi.Models.Domain;
using webapi.Models.DTO;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WebhookSubscriptionsController : ControllerBase
    {
        private readonly IWebhookSubscriptionRepository _WebhookSubscriptionRepository;
        private readonly IMapper _mapper;

        public WebhookSubscriptionsController(IWebhookSubscriptionRepository webhookSubscriptionRepository, IMapper mapper)
        {
            _WebhookSubscriptionRepository = webhookSubscriptionRepository;
            _mapper = mapper;
        }


        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] int page, [FromQuery] int limit)
        {
            var result = await _WebhookSubscriptionRepository.GetAllAsync(page, limit);

            var totalCount = await _WebhookSubscriptionRepository.GetCount();

            var resultDto = _mapper.Map<IList<WebhookSubscriptionDto>>(result);

            var paginationResponseDto = new PaginationResponseDto()
            {
                Results = resultDto,
                TotalCount = totalCount

            };

            return Ok(paginationResponseDto);
        }

        [HttpGet("{id}", Name = "GetWebhookSubscription")]
        public async Task<IActionResult> GetById(int id)
        {
            var subscription = await _WebhookSubscriptionRepository.GetByIDAsync(id);
            if (subscription == null)
            {
                return NotFound();
            }

           
            var resultDto = _mapper.Map<WebhookSubscriptionDto>(subscription);

            return Ok(resultDto);
        }

        [HttpPost]
        public async Task<IActionResult> Create(WebhookSubscriptionDto subscription)
        {
            var webhookSubscription = _mapper.Map<WebhookSubscription>(subscription);

            await _WebhookSubscriptionRepository.CreateAsync(webhookSubscription);

            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, WebhookSubscriptionDto updatedSubscription)
        {
            var subscription = await _WebhookSubscriptionRepository.GetByIDAsync(id);
            if (subscription == null)
            {
                return NotFound();
            }

            subscription.CallbackUrl = updatedSubscription.CallbackUrl;
            subscription.Name = updatedSubscription.Name;

            await _WebhookSubscriptionRepository.UpdateAsync(subscription);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var subscription = await _WebhookSubscriptionRepository.GetByIDAsync(id);
            if (subscription == null)
            {
                return NotFound();
            }

            await  _WebhookSubscriptionRepository.DeleteAsync(subscription);

            return NoContent();
        }
    }
}
