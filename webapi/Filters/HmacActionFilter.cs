﻿using Azure.Core;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using webapi.Extensions;

namespace HMACAuthenticationWebApi.Models
{
    public class HmacActionFilter : IActionFilter
    {
        private readonly string _secretKey; // Aquí debes usar tu clave secreta
        private readonly UInt64 requestMaxAgeInSeconds = 300; //Means 5 min
        private readonly string authenticationScheme = "hmacauth";

        public HmacActionFilter(string secretKey)
        {
            _secretKey = secretKey;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var request = context.HttpContext.Request;
            var auto = request.Headers.Authorization;

 
            //var bodyStr = ReadBodyAsync(context.HttpContext.Request.Body).Result;
            var requestBody = request.BodyToString();

            var rawAuthzHeader = request.Headers.Authorization.FirstOrDefault();
            if (rawAuthzHeader != null && rawAuthzHeader.StartsWith($"{authenticationScheme} ", StringComparison.OrdinalIgnoreCase))
            {
                var authValues = rawAuthzHeader.Substring(9).Split(':');
                if (authValues.Length == 3)
                {
                    var timestamp = authValues[0];
                    var nonce = authValues[1];
                    var requestRmac = authValues[2];
                    // Process the timestamp, nonce, and hmac here
                    if (isReplayRequest(nonce, timestamp))
                    {
                        context.Result = new Microsoft.AspNetCore.Mvc.UnauthorizedObjectResult("Invalid HMAC");
                    }

                    var hmac = CalculateHmac(requestBody, _secretKey);

                    if (!string.Equals(hmac, requestRmac))
                    {
                        // If the HMAC does not match, you can handle the error here
                        context.Result = new Microsoft.AspNetCore.Mvc.UnauthorizedObjectResult("Invalid HMAC");
                    }
                }
                else
                {
                    // Handle the case when the Authorization header does not contain valid hmacauth values
                    context.Result = new Microsoft.AspNetCore.Mvc.UnauthorizedObjectResult("Invalid HMAC");
                }
            } else
            {
                context.Result = new Microsoft.AspNetCore.Mvc.UnauthorizedObjectResult("Invalid HMAC");
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // Aquí puedes realizar alguna lógica después de la ejecución de la acción si es necesario
        }

        private string CalculateHmac(string message, string secret)
        {
            var encoding = new ASCIIEncoding();
            byte[] keyBytes = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyBytes))
            {
                byte[] hashMessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashMessage);
            }
        }

        private bool isReplayRequest(string nonce, string requestTimeStamp)
        {
            if (System.Runtime.Caching.MemoryCache.Default.Contains(nonce))
            {
                return true;
            }

            DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan currentTs = DateTime.UtcNow - epochStart;

            var serverTotalSeconds = Convert.ToUInt64(currentTs.TotalSeconds);
            var requestTotalSeconds = Convert.ToUInt64(requestTimeStamp);

            if ((serverTotalSeconds - requestTotalSeconds) > requestMaxAgeInSeconds)
            {
                return true;
            }

            System.Runtime.Caching.MemoryCache.Default.Add(nonce, requestTimeStamp, DateTimeOffset.UtcNow.AddSeconds(requestMaxAgeInSeconds));

            return false;
        }
    }
}