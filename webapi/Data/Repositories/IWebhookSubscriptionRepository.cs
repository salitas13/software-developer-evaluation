﻿using System.Collections.Generic;
using webapi.Models.Domain;

namespace webapi.Data.Repositories
{
    public interface IWebhookSubscriptionRepository
    {
        public Task DeleteAsync(WebhookSubscription webhookSubscription);
        public Task<WebhookSubscription> CreateAsync(WebhookSubscription webhookSubscription);
        public Task UpdateAsync(WebhookSubscription webhookSubscription);
        public Task<IList<WebhookSubscription>> GetAllAsync(int page, int limit);
        public Task<int> GetCount();
        public Task<WebhookSubscription> GetByIDAsync(int id);
    }
}
