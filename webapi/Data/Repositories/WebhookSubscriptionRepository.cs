﻿using Microsoft.EntityFrameworkCore;
using webapi.Models.Domain;

namespace webapi.Data.Repositories
{
    public class WebhookSubscriptionRepository : IWebhookSubscriptionRepository
    {
        readonly ApplicationDbContext _dbContext;
        public WebhookSubscriptionRepository(ApplicationDbContext dbContext) { 
            this._dbContext = dbContext;
        }

        public async Task<WebhookSubscription> CreateAsync(WebhookSubscription webhookSubscription)
        {
            await _dbContext.AddAsync(webhookSubscription);
            await _dbContext.SaveChangesAsync();
            return webhookSubscription;
        }

        public async Task<IList<WebhookSubscription>> GetAllAsync(int page, int limit)
        {
            var results = await _dbContext.WebhookSubscriptions
                .Skip((page - 1) * limit) // Skip elements on previous pages
                .Take(limit) // Take only the specified amount of elements for the current page
                .ToListAsync(); // Convert to list
            return results;
        }

        public async Task<WebhookSubscription> GetByIDAsync(int id)
        {
            var result = await _dbContext.WebhookSubscriptions.FirstOrDefaultAsync(t => t.Id == id);
            return result;
        }

        public async Task UpdateAsync(WebhookSubscription webhookSubscription)
        {
            _dbContext.Update(webhookSubscription);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(WebhookSubscription webhookSubscription)
        {
            _dbContext.WebhookSubscriptions.Remove(webhookSubscription);
            await _dbContext.SaveChangesAsync();
        }

        public Task<int> GetCount()
        {
            return _dbContext.WebhookSubscriptions.CountAsync();
        }
    }
}
