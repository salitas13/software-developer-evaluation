﻿namespace webapi.Models.Domain
{
    public class WebhookSubscription
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CallbackUrl { get; set; }
    }
}
