﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using webapi.Models.Domain;
using webapi.Models.DTO;

namespace webapi.Models
{
    public class AutoMapper : Profile
    {
        public AutoMapper()
        {
            CreateMap<WebhookSubscription, WebhookSubscriptionDto>();
            CreateMap<WebhookSubscriptionDto, WebhookSubscription>();
        }
    }
}
