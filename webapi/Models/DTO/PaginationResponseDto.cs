﻿using System.Text.Json.Serialization;

namespace webapi.Models.DTO
{
    public class PaginationResponseDto
    {
        public IList<WebhookSubscriptionDto> Results { get; set; }
        [JsonPropertyName("totalcount")]
        public int TotalCount { get; set; }
    }
}
