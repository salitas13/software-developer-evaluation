﻿using System.Text.Json.Serialization;

namespace webapi.Models.DTO
{
    public class WebhookSubscriptionDto
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("callbackurl")]
        public string CallbackUrl { get; set; }
    }
}
