// webhook.model.ts
export interface Webhook {
  id: number;
  name: string;
  callbackurl: string;
}
