import { Webhook } from './webhook';

export interface PaginationResponse {
  results: Webhook[];
  totalcount: number;
}
