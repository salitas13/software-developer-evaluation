import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Webhook } from '../models/webhook';
import { PaginationResponse } from '../models/paginationresponse';
import { Observable } from 'rxjs';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebhookService {
  private baseUrl = 'https://localhost:7213/api/webhooksubscriptions';

  constructor(private http: HttpClient) { }

  getAllWebhooks = (page = 1, limit = 10) =>
    this.http.get(this.baseUrl + `?page=${page}&limit=${limit}`, { observe: 'response' })
      .pipe(
        map(response => {
          const data = response.body as PaginationResponse;
          const webhookssuscriptions = data.results;
          const count = data.totalcount;
          
          return { webhookssuscriptions, count }
        })
    )

  getWebhookById = (id: number) => 
      this.http.get<Webhook>(`${this.baseUrl}/${id}`);
  

  createWebhook(webhook: Webhook, signature: string): Observable<Webhook> {
    const headers = new HttpHeaders().set('HMAC-Signature', signature);
    if (webhook.id == 0 || webhook.id == null) {
      webhook.id = 0;
      return this.http.post<Webhook>(this.baseUrl, webhook, { headers });
    } else {
      return this.http.put<Webhook>(`${this.baseUrl}/${webhook.id}`, webhook, { headers });
    }
  }

  deleteWebhook = (id: number, signature: string) => {
    const headers = new HttpHeaders().set('HMAC-Signature', signature);
    return this.http.delete(`${this.baseUrl}/${id}`, { headers });
  }
}
