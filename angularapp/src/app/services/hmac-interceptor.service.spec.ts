import { TestBed } from '@angular/core/testing';

import { HmacInterceptorService } from './hmac-interceptor.service';

describe('HmacInterceptorService', () => {
  let service: HmacInterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HmacInterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
