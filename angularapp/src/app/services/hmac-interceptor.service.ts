import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import * as CryptoJS from 'crypto-js';
import { Observable } from 'rxjs';
import { Guid } from 'guid-typescript';

@Injectable()
export class HmacInterceptorService implements HttpInterceptor {
  secretKey = 'e2739_x-qdvil_u7)d*e-!=wb64kgw_@h&368b6g&*&fabe@*6'; // Replace with your actual secret key

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const timestamp = Math.floor(Date.now() / 1000).toString();
    const nonce = this.generateNonce();

    const content = (req.body ? JSON.stringify(req.body) : '');
    const hmac = CryptoJS.HmacSHA256(content, this.secretKey).toString(CryptoJS.enc.Base64);

    const authHeader = `hmacauth ${timestamp}:${nonce}:${hmac}`;
    const authReq = req.clone({ setHeaders: { Authorization: authHeader } });

    return next.handle(authReq);
  }

  private generateNonce(): string {
    // Implement your nonce generation logic here
    return Guid.create().toString();
  }
}
