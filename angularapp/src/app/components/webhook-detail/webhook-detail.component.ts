import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { WebhookService } from '../../services/webhook.service';
import { Webhook } from '../../models/webhook';
import { MyErrorStateMatcher } from '../../models/MyErrorStateMatcher';

@Component({
  selector: 'app-webhook-detail',
  templateUrl: './webhook-detail.component.html',
  styleUrls: ['./webhook-detail.component.css']
})
export class WebhookDetailComponent implements OnInit {
  isEditMode = false;
  frm!: FormGroup;

  @ViewChild("webhookForm") whForm!: NgForm; // it will be used for resetting the form validation messages

  get f() {
    return this.frm.controls;
  }
  errorMatcher = new MyErrorStateMatcher();

  constructor(
    private route: ActivatedRoute,
    private webhookService: WebhookService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.isEditMode = true;
      this.webhookService.getWebhookById(parseInt(id, 10)).subscribe({
        next: (webhook => this.frm.patchValue(webhook)),
        error: (err) => console.log(err)
      });
    }
    this.frm = this.fb.group({
      id: null,
      name: ['', Validators.required],
      callbackurl: ['', [Validators.required]]
    })
  }

  saveWebhook(): void {
    this.webhookService.createWebhook(this.frm.value, 'hmacSignature').subscribe({
      next: (data) => {
        this.whForm.reset();
        this.whForm.resetForm();
        this.snackBar.open("success", 'close', {
          duration: 3000
        })
      },
      error: (err) => {
        console.log(err);
        this.snackBar.open("error", 'close', {
          duration: 3000
        })
      }
    }
    )
  }

  goBack(): void {
    this.router.navigate(['webhooks']);
  }
}
