import { Component, OnInit } from '@angular/core';
import { WebhookService } from '../../services/webhook.service';
import { Webhook } from '../../models/webhook';
import { PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-webhook-list',
  templateUrl: './webhook-list.component.html',
  styleUrls: ['./webhook-list.component.css']
})
export class WebhookListComponent implements OnInit {
  dataSource = new MatTableDataSource<Webhook>();
  displayColumns = ['id', 'name', 'callbackurl', 'action'];
  pageSizeOptions = [3, 6, 9, 12];
  pageSize = 3;
  pageIndex = 0
  pageLength = 0; //total records in db

  constructor(private webhookService: WebhookService) { }

  ngOnInit(): void {
    this.fetchWebhooks();
  }

  changePage(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.fetchWebhooks();
  }

  fetchWebhooks() {
    this.webhookService.getAllWebhooks((this.pageIndex + 1), this.pageSize).subscribe({
      next: (response) => {
        this.dataSource.data = response.webhookssuscriptions;
        this.pageLength = response.count; 
      },
      error: (err) => console.log(err)
    });
  }

  deleteWebhook(id: number): void {
    if (window.confirm('Are you sure to delete?')) {
      {
        this.webhookService.deleteWebhook(id, 'hmacSignature').subscribe({
          next: (resp) => {
            if (this.dataSource.data.length === 1 && this.pageIndex > 0)
              this.pageIndex--;
            this.fetchWebhooks();
          },
          error: (err) => console.log(err)
        })
      }
    }
  }
}
