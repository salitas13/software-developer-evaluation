﻿using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using webapi.Controllers;
using webapi.Data.Repositories;
using webapi.Models.Domain;
using webapi.Models.DTO;

namespace webapi.test.Controllers
{
    public class WebhookSubscriptionsControllerTest
    {
        [Fact]
        public void GetAll_HMACHeader_Success()
        {
            // Arrange
            var mappingService = new Mock<IMapper>();
            var mockRepository = new Mock<IWebhookSubscriptionRepository>();

            var webhookSubscriptionsExpected = new List<WebhookSubscription>()
            {
                 new WebhookSubscription { Id = 1, Name = "Webhook 1", CallbackUrl = "Url 1" },
                 new WebhookSubscription { Id = 1, Name = "Webhook 2", CallbackUrl = "Url 2" }
            };

            var webhookSubscriptionsExpectedDto = webhookSubscriptionsExpected.Select(t => new WebhookSubscriptionDto()).ToList();

            var paginationResponseDto = new PaginationResponseDto()
            {
                Results = webhookSubscriptionsExpectedDto,
                TotalCount = webhookSubscriptionsExpected.Count
            };

            int page = 1;
            int limit = 10;
            mockRepository.Setup(x => x.GetAllAsync(page, limit))
                .Returns(Task.FromResult<IList<WebhookSubscription>>(webhookSubscriptionsExpected));

            mappingService.Setup(m => m.Map<IList<WebhookSubscription>, IList<WebhookSubscriptionDto>>(webhookSubscriptionsExpected)).Returns(webhookSubscriptionsExpectedDto);

            // Act
            var controller = new WebhookSubscriptionsController(mockRepository.Object, mappingService.Object);
            IHttpActionResult actionResult = (IHttpActionResult)controller.GetAll(page, limit);
            var contentResult = actionResult as OkNegotiatedContentResult<PaginationResponseDto>;

            // Assert
            Assert.NotNull(contentResult);
            Assert.NotNull(contentResult.Content);
            Assert.Equal(paginationResponseDto.TotalCount, contentResult.Content.TotalCount);
        }

        [Fact]
        public void GetAll_Without_HMACHeader_Fail()
        {
            // Arrange
            var mappingService = new Mock<IMapper>();
            var mockRepository = new Mock<IWebhookSubscriptionRepository>();

            var webhookSubscriptionsExpected = new List<WebhookSubscription>()
            {
                 new WebhookSubscription { Id = 1, Name = "Webhook 1", CallbackUrl = "Url 1" },
                 new WebhookSubscription { Id = 1, Name = "Webhook 2", CallbackUrl = "Url 2" }
            };

            var webhookSubscriptionsExpectedDto = webhookSubscriptionsExpected.Select(t => new WebhookSubscriptionDto()).ToList();

            var paginationResponseDto = new PaginationResponseDto()
            {
                Results = webhookSubscriptionsExpectedDto,
                TotalCount = webhookSubscriptionsExpected.Count
            };

            int page = 1;
            int limit = 10;
            mockRepository.Setup(x => x.GetAllAsync(page, limit))
                .Returns(Task.FromResult<IList<WebhookSubscription>>(webhookSubscriptionsExpected));

            mappingService.Setup(m => m.Map<IList<WebhookSubscription>, IList<WebhookSubscriptionDto>>(webhookSubscriptionsExpected)).Returns(webhookSubscriptionsExpectedDto);

            // Act
            var controller = new WebhookSubscriptionsController(mockRepository.Object, mappingService.Object);
            var actionResult = controller.GetAll(page, limit);

            // Assert
            Assert.NotNull(actionResult);
            Assert.IsType<UnauthorizedResult>(actionResult);
        }
    }
}
